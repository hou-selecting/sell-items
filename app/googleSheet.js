// googleSheet.js

const { GoogleSpreadsheet } = require('google-spreadsheet');
const requests = require("request");

/**
 * @param  {String} docID the document ID
 * @param  {String} sheetID the google sheet table ID
 * @param  {String} credentialsPath the credentials path defalt is './credentials.json'
 */
async function readGoogleSheet(docID, sheetID, credentialsPath = './data/credentials.json') {
  const result = [];
  const doc = new GoogleSpreadsheet(docID);
  const creds = require(credentialsPath);
  await doc.useServiceAccountAuth(creds);
  await doc.loadInfo();
  const sheet = doc.sheetsById[sheetID];
  const rows = await sheet.getRows();
  for (row of rows) {
    result.push(row._rawData);
  }
  return result;
};

/**
 * @param  {String} docID the document ID
 * @param  {String} sheetName the google sheet Name
 * @param  {String} data the google sheet data
 * @param  {String} credentialsPath the credentials path defalt is './credentials.json'
 */
async function writeGoogleSheet(docID, sheetID, data, credentialsPath = './data/credentials.json') {
  const sheets = new GoogleSpreadsheet(docID);
  const creds = require(credentialsPath);
  await sheets.useServiceAccountAuth(creds);
  await sheets.loadInfo();
  const overwriteRange = "Sheet1!A1"; 

  requests.put(
    {
      url: `https://sheets.googleapis.com/v4/spreadsheets/${doc.spreadsheetId}/values/${overwriteRange}?valueInputOption=USER_ENTERED`,
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${doc.jwtClient.credentials.access_token}`,
      },
      body: JSON.stringify({ data: data }),
    },
    (err, res, body) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log(body);
    });

};

module.exports = {
  readGoogleSheet,
  writeGoogleSheet
};