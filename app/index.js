const fs = require('fs')
const jinqJs = require('jinq')
const axios = require('axios')
const { writeGoogleSheet } = require('./googleSheet.js');

const remoteUrl = 'https://shopee.tw/api/v4/search/search_items?by=price&limit=100&match_id=396528434&newest=0&order=desc&page_type=shop&scenario=PAGE_SHOP_CATEGORY_SEARCH&shop_categoryids=1&version=2'

//const obj = JSON.parse(fs.readFileSync('./data/sample1.json', 'utf8'))

axios.get(remoteUrl)
  .then((response) => {

    let data = response.data.items.map(item => ({
      'name': nameProcess(item.item_basic.name),
      'price': item.item_basic.price_max / 100000,
      'brand': item.item_basic.brand,
      'image': 'https://cf.shopee.tw/file/' + item.item_basic.image
    }))

    let result = new jinqJs().from(data).orderBy([{
      field: 'price',
      sort: 'desc'
    }, {
      field: 'name'
    }]).select()

    writeGoogleSheet('1Xlnpm9qPbdEnEOGuYImDZmt6L7Fg813DnEHM8HpLaoM','老船長',result)
    //saveToJson(convertToCSV(result))

  }).catch((err) => {
    console.log(err)
  })

function nameProcess(currentName) {

  const lastKeyword = '】'
  let result = currentName.substring(0, currentName.indexOf(lastKeyword) + 1).replace('【', '').replace('】', '')

  return result
}

function saveToJson(input) {
  fs.writeFile('./out/data.csv', input, function (err, data) {
    if (err) {
      return console.log(err);
    }
  });
}

function convertToCSV(objArray) {
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  let result = '';
  let titleColumnsLine = '';
  
  for(let key in array[0]){
    if (titleColumnsLine != '') titleColumnsLine += ','

    titleColumnsLine += key;
  }

  result += titleColumnsLine + '\r\n';
  
  for (let i = 1; i < array.length+1; i++) {
      let line = '';
      for (let index in array[i]) {
          if (line != '') line += ','

          line += array[i][index];
      }

      result += line + '\r\n';
  }

  return result;
}